import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const AccueilStack = createStackNavigator();

import Accueil from '../Accueil/Accueil';

// Page stack accueil : gestion de la navigation dans la fonctionnalité Accueil

const StackAccueil = () => {
  return (
    <AccueilStack.Navigator screenOptions={{ headerShown: true }}>
      <AccueilStack.Screen name="Accueil" component={Accueil} />
    </AccueilStack.Navigator>
  );
}

export default StackAccueil;