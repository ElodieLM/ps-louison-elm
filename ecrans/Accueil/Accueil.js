import React from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Provider } from 'react-redux';

import Titre from '../../composants/Titre';
import Ville from '../../composants/Ville';
import CardsData from '../../composants/CardsData';
import CardsTrajetEquipe from '../../composants/CardsTrajetEquipe';
import ListeBilans from '../../composants/ListeBilans';
import store from '../../store/index';
import Meteo from '../../composants/Meteo';

// Page Accueil : ecran principal de l'application

const Accueil = () => {

  return (
    <Provider store={store}>
      <ScrollView>
        <Titre style={styles.fontStyleTitre} value="Hello Cam !" />
        <Ville />
        <CardsData values={["14.4", "24.2 €", "843"]} unites={["Kg Co2*", "économie*", "kCalories*"]} />
        <CardsTrajetEquipe />
        <Meteo />
        <ListeBilans />
      </ScrollView>
    </Provider >
  )
}

export default Accueil;

const styles = StyleSheet.create({
  fontStyle: {
    fontSize: '30px',
    color: 'red'
  },
  fontStyleTitre: {
    fontSize: '24px',
  }
});