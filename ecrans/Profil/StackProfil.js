import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const NavigationStack = createStackNavigator();

import Profil from '../Profil/Profil';

// Page stack profil : gestion de la navigation dans la fonctionnalité Profil

const StackProfil = () => {
  return (
    <NavigationStack.Navigator screenOptions={{ headerShown: true }}>
      <NavigationStack.Screen name="Profil" component={Profil} />
    </NavigationStack.Navigator>
  );
}

export default StackProfil;