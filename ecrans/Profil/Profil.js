import React from 'react';
import { View, Button, Text } from 'react-native';
import { Provider } from 'react-redux';

import store from '../../store/index';

// Page Profil : gestion de la navigation dans la fonctionnalité Equipe

const Profil = ({ navigation }) => {
  return (
    <Provider store={store}>

      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Profil</Text>
        <Button
          title="Go to ici"
          onPress={() => navigation.navigate('Profil')}
        />
      </View>
    </Provider>
  )
}

export default Profil;