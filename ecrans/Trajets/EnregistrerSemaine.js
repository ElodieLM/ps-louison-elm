import React, { useState } from 'react';
import { StyleSheet, CheckBox, View, Button, Text, Image, ScrollView } from 'react-native';
import { Provider } from 'react-redux';

import store from '../../store/index';
import APP_COLORS from '../../constantes/Colors';

// Page EnregistrerSemaine

// Ensemble de check box permettant de définir les jours pendant lesquels l'utilisateur à fait du vélo

const EnregistrerSemaine = ({ navigation }) => {
  const [isMondaySelected, setMonday] = useState(false);
  const [isTuesdaySelected, setTuesday] = useState(false);
  const [isWednesdaySelected, setWednesday] = useState(false);
  const [isThursdaySelected, setThursday] = useState(false);
  const [isFridaySelected, setFriday] = useState(false);
  const [isSaturdaySelected, setSaturday] = useState(false);
  const [isSundaySelected, setSunday] = useState(false);

  const selectionSemaine = [{ 'value': isMondaySelected }, { 'value': isTuesdaySelected }, { 'value': isWednesdaySelected },
  { 'value': isThursdaySelected }, { 'value': isFridaySelected }, { 'value': isSaturdaySelected }, { 'value': isSundaySelected }];

  return (
    <Provider store={store}>
      <ScrollView style={{ backgroundColor: "white", padding: "20px", }}>

        <View>
          <Text style={styles.titre3}>Enregistrez <Text style={styles.spanTitre}>votre semaine</Text> à vélo</Text>
          <Image style={styles.imageSemaine} source={require('../../assets/images/enregistrementSemaine.png')} />
          <Text style={{ fontSize: "18px", marginTop: "20px" }}>Jour à vélo</Text>
          <View style={styles.checkboxSuperContainer}>
            <View style={styles.checkboxContainer}>
              <View style={styles.input}>
                <CheckBox
                  value={isMondaySelected}
                  onValueChange={setMonday}
                  style={styles.checkbox}
                  Text="test"
                />
                <Text style={styles.label}>Lundi</Text>
              </View>
              <View style={styles.input}>
                <CheckBox
                  value={isTuesdaySelected}
                  onValueChange={setTuesday}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Mardi</Text>
              </View>
              <View style={styles.input}>
                <CheckBox
                  value={isWednesdaySelected}
                  onValueChange={setWednesday}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Mercredi</Text>
              </View>
              <View style={styles.input}>
                <CheckBox
                  value={isThursdaySelected}
                  onValueChange={setThursday}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Jeudi</Text>
              </View>
            </View>
            <View style={styles.checkboxContainer}>
              <View style={styles.input}>
                <CheckBox
                  value={isFridaySelected}
                  onValueChange={setFriday}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Vendredi</Text>
              </View>
              <View style={styles.input}>
                <CheckBox
                  value={isSaturdaySelected}
                  onValueChange={setSaturday}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Samedi</Text>
              </View>
              <View style={styles.input}>
                <CheckBox
                  value={isSundaySelected}
                  onValueChange={setSunday}
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Dimanche</Text>
              </View>
            </View>
          </View>
        </View>

        <Button
          title="Suivant"
          // transmet le planning de la semaine à l'écran suivant
          onPress={() => { navigation.navigate('Trajets2', selectionSemaine) }}
        />
      </ScrollView>

    </Provider >
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: "100%",
    justifyContent: "flex-start",
    margin: "20px",
  },
  checkboxContainer: {
    flexDirection: "column",
    marginTop: "10px",
    marginBottom: "20px",
    width: "50%",
  },
  label: {
    fontSize: "20px",
    marginLeft: "10px",
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: "35px",
  },
  checkboxSuperContainer: {
    flexDirection: "row",
    marginBottom: 20,
    maxWidth: "300px",
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  checkbox: {
    alignSelf: "left",
  },
  titre3: {
    fontSize: '24px',
    marginTop: '30px',
    marginBottom: '50px'
  },
  spanTitre: {
    position: "relative",
  },
  "spanTitre:before": {
    position: "absolute",
    conten: "",
    backgroundColor: APP_COLORS.primaryYellow,
    width: "100%",
    height: "10px",
    top: 0,
    bottom: 0,
  },
  imageSemaine: {
    width: "350px",
    height: "120px",
    position: 'relative'
  }
});


export default EnregistrerSemaine;