import React, { useState } from 'react';

import { View, Button, Text, CheckBox, StyleSheet, Picker, ScrollView, TouchableOpacity, Image } from 'react-native';
import { Provider } from 'react-redux';
import store from '../../store/index';

import { useNavigation } from '@react-navigation/native';

import CarteEditionItineraire from '../../composants/CarteEditionItineraire';

// Page ModificationCarteTrajet
// Permet de modifier les trajets effectuer dans la semaine

const ModificationCarteTrajet = ({ route }) => {
  const navigation = useNavigation();
  const listeTrajets = ['Vélotaf'];
  const listeCartes = [];

  // Génère la liste listeCartes en fonction de la liste des itinéraires enregistrés
  for (let index = 0; index < listeTrajets.length; index++) {
    listeCartes[index] = { id: 'carte' + index, name: "name" + index };
  }

  // Ajout d'une nouvelle carte dans la liste listeCartes
  function addCard() {
    listeCartes.push({ id: 'carte' + listeTrajets.length + 1, name: 'name' + listeTrajets.length + 1 })
    console.log(listeCartes);
  }

  // Affiche les composants CarteEditionItineraire en fonction de la liste listeCartes
  function renderCartes() {
    return listeCartes.map((item) => {
      return (
        <CarteEditionItineraire key={item.id} />
      )
    })
  }

  return (
    <Provider store={store}>
      <ScrollView>
        <View>
          <Text style={styles.bloc}>Modifier votre trajet du {route.params[0]}</Text>

          {renderCartes()}

          {/* <Button
            title="Ajouter un trajet"
            onPress={() => addCard()}
          /> */}
          <TouchableOpacity onPress={() => addCard()}>
            <Image source={require('../../assets/images/icons/oeuf.png')}></Image>
            <Text>Ajouter un trajet</Text>
          </TouchableOpacity>

          <Button
            title="Suivant"
            onPress={() => navigation.goBack()}
          />
        </View>
      </ScrollView>
    </Provider >
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    alignItems: "center"
  },
  bloc: {
    marginTop: "20px",
    marginBottom: "40px",
    paddingLeft: "20px",
    paddingRight: "20px"
  }
});

export default ModificationCarteTrajet;