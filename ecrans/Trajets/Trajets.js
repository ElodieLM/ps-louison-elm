import React from 'react';
import { View, Button, Text } from 'react-native';
import { Provider } from 'react-redux';
import store from '../../store/index';

// Page Trajets
// Work in progress
// Ecran principal de la fonctionnalité Trajets

const Trajets = ({ navigation }) => {
  return (
    <Provider store={store}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Trajets</Text>
        <Button
          title="Enregistrer ma semaine"
          onPress={() => navigation.navigate('EnregistrerSemaine')}
        />
      </View>
    </Provider>
  )
}

export default Trajets;