import React from 'react';

import { ScrollView, Button, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import store from '../../store/index';

import Titre from '../../composants/Titre';
import TrajetCard from '../../composants/TrajetCard';

// Page récapituliatif de la semaine
// Permet de valider les trajets de la semaine et d'acceder à la page de modification si besoin

const Trajets2 = ({ route, navigation }) => {

  const selectionSemaine = route.params;

  const jourSemaine = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
  const listeCartes = [];
  const listeTrajetsJour = ['2 Trajets - 5 km', '2 Trajets - 5 km', '2 Trajets - 5 km', '2 Trajets - 5 km', '2 Trajets - 5 km', '2 Trajets - 5 km', '2 Trajets - 5 km'];


  // Génère la liste listeCartes en fonction de la liste des jours selectionnés
  for (let index = 0; index < selectionSemaine.length; index++) {
    const element = selectionSemaine[index];

    if (element.value) {
      listeCartes.push({ jour: jourSemaine[index], trajet: listeTrajetsJour[index], id: index });
    }
  }
  
  // Affiche les composants TrajetCard en fonction de la liste listeCartes
  function renderCartes() {
    return listeCartes.map((item) => {
      return (
        <TrajetCard key={item.id} jour={item.jour} trajet={item.trajet} />
      )
    })
  }

  return (
    <Provider store={store}>
      <Titre style={styles.fontStyleTitre} value="Le récap de la semaine" />
      <ScrollView>
        {renderCartes()}
      </ScrollView>
      <Button
        title="Go to ici"
        onPress={() => navigation.navigate('Accueil')}
      />
    </Provider >
  )
}

const styles = StyleSheet.create({
  fontStyle: {
    fontSize: '30px',
    color: 'red'
  },
  fontStyleTitre: {
    fontSize: '24px',
    marginTop: '20px'
  }
});

export default Trajets2;