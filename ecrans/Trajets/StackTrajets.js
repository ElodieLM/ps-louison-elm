import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const NavigationStack = createStackNavigator();

import Trajets from '../Trajets/Trajets';
import Trajets2 from '../Trajets/Trajets2';
import Trajets3 from '../Trajets/Trajets3';
import EnregistrerSemaine from '../Trajets/EnregistrerSemaine';
import ModificationCarteTrajet from '../Trajets/ModificationCarteTrajet';

// Page stack trajets : gestion de la navigation dans la fonctionnalité Trajets

const StackTrajets = () => {
  return (
    <NavigationStack.Navigator screenOptions={{ headerShown: true }}>
      <NavigationStack.Screen name="Trajets" component={Trajets} />
      <NavigationStack.Screen name="Trajets2" component={Trajets2} />
      <NavigationStack.Screen name="Trajets3" component={Trajets3} />
      <NavigationStack.Screen name="EnregistrerSemaine" component={EnregistrerSemaine} />
      <NavigationStack.Screen name="ModificationCarteTrajet" component={ModificationCarteTrajet} />
    </NavigationStack.Navigator>
  );
}

export default StackTrajets;