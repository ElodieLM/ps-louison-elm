import React from 'react';

import { View, Button, Text } from 'react-native';
import { Provider } from 'react-redux';
import store from '../../store/index';

// Ecran inutilisé pour l'instant

const Trajets3 = ({ navigation }) => {
  return (
    <Provider store={store}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Trajets3</Text>
        <Button
          title="Suivant"
          onPress={() => navigation.navigate('Trajets')}
        />
      </View>
    </Provider>
  )
}

export default Trajets3;