import React from 'react';
import { View, Button, Text } from 'react-native';
import { Provider } from 'react-redux';
import store from '../../store/index';

// Page Equipe : ecran principal de la fonctionnalité Equipe

const Equipe = ({ navigation }) => {
  return (
    < Provider store={store} >
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Equipe</Text>
        <Button
          title="Go to ici"
          onPress={() => navigation.navigate('Equipe')}
        />
      </View>
    </Provider >
  )
}

export default Equipe;