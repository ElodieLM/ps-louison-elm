import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const NavigationStack = createStackNavigator();

import Equipe from '../Equipe/Equipe';

// Page stack equipe : gestion de la navigation dans la fonctionnalité Equipe

const StackEquipe = () => {
  return (
    <NavigationStack.Navigator screenOptions={{ headerShown: true }}>
      <NavigationStack.Screen name="Equipe" component={Equipe} />
    </NavigationStack.Navigator>
  );
}

export default StackEquipe;