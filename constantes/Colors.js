const APP_COLORS = {
    white: '#fff',
    black: '#000',
    primaryYellow: '#F4BB44',
    secondaryYellow: '#FFE36D',
    primaryBlue: '#99ACD5',
    secondaryBlue: '#C4D3EB',
    thirdBlue: '#D6E6F5',
    primaryDarkBlue: '#353B51',
    secondaryDarkBlue: '#4E5367'
};

export default APP_COLORS;