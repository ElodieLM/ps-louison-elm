import React from 'react';
import { Text, StyleSheet, Image, View } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';
import CardData from '../composants/CardData';

const CardsData = (params) => {
    return (
        <View>
            <View style={styles.cardsStyle}>
                <CardData value={params.values[0]} unite={params.unites[0]} />
                <CardData value={params.values[1]} unite={params.unites[1]} />
                <CardData value={params.values[2]} unite={params.unites[2]} />
            </View>
            <Text style={{ textAlign: 'right', marginRight: '10px' }}>* par semaine</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    cardsStyle: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: '-40px'
    }
});

export default CardsData;
