import React from 'react';
import { StyleSheet, Image, View, Button, Text } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';

const Bilan = () => {
    return (
        <View style={styles.bilan}>
            <Text style={styles.titreBilan}>Bilan Avril</Text>
            <Image style={styles.imageBilan} source={require('../assets/images/icons/oeuf.png')} />
            <Button
                title="Decouvrir"
                onPress={() => navigation.navigate('Equipe')}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    bilan: {
        width: '230px',
        backgroundColor: APP_COLORS.primaryYellow,
        padding: '20px',
        borderRadius: '15px',
        marginRight: '20px',
    },
    titreBilan: {
        fontSize: '18px',
        marginBottom: '30px'
    },
    imageBilan: {
        width: '50px',
        height: '50px',
        position: 'absolute',
        top: '10px',
        right: '20px',
    }
});

export default Bilan;