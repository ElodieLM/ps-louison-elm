import React from 'react';
import { Text, StyleSheet, Image, View, Button } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';
import CardData from '../composants/CardData';

const CardTrajet = () => {
    return (
        <View style={styles.cardTrajetStyle}>
            <Image style={styles.imageTrajet} source={require('../assets/images/illu-trajet.png')} />
            <View style={styles.resultat}>
                <Text style={styles.points}>1 035</Text>
                <Text style={styles.unite}>pts</Text>
            </View>
            <View style={styles.bouton}>
                <Button
                    title="Trajet"
                    onPress={() => navigation.navigate('Trajet')}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cardTrajetStyle: {
        position: 'relative',
        backgroundColor: APP_COLORS.secondaryBlue,
        width: '45%',
        height: '200px',
        borderRadius: '15px',
    },
    imageTrajet: {
        height: '70px',
        width: '70px',
        margin: '5px'
    },
    resultat: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        left: '55px',
        top: '-10px'
    },
    points: {
        fontSize: '25px',
    },
    unite: {
        fontSize: '16px',
        marginLeft: '4px'
    },
    bouton: {
        position: 'absolute',
        bottom: '25px',
        left: '50%',
        transform: [{ translate: '-50%' }]
    }
});


export default CardTrajet;
