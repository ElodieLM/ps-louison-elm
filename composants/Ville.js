import React from 'react';
import { StyleSheet, Image } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';


const Ville = () => {
    return (
        <Image style={styles.imageVille} source={require('../assets/images/quartier-rond1.png')} />
    )
}

const styles = StyleSheet.create({
    imageVille: {
        width: '100%',
        height: '430px',
    },
});

export default Ville;