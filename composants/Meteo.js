import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';

const Meteo = () => {
    return (
        <View style={styles.meteo}>
            <View style={styles.txtMeteo}>
                <Text style={{ fontSize: '18px', marginTop: '5px', marginBottom: '15px' }}>Meteo</Text>
                <Text style={{ fontSize: '18px', marginBottom: '7px' }}>Lundi 17 Mai</Text>
                <Text style={{ fontSize: '16px' }}>17h à 18h</Text>
            </View>
            <View style={styles.resultatMeteo}>
                <View style={styles.blanc}></View>
                <Text style={styles.temperature}>12°C</Text>
                <Image style={styles.imageMeteo} source={require('../assets/images/icons/sun.png')} />
                <Text style={{ zIndex: '2', marginTop: '6px' }} >Ensoleillé</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    meteo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 'calc( 100% - 20px)',
        height: '100px',
        marginTop: '20px',
        backgroundColor: APP_COLORS.secondaryBlue,
        marginLeft: '10px',
        marginRight: '10px',
        borderRadius: '15px',
        paddingLeft: '15px',
        paddingRight: '15px',
        overflow: 'hidden',
    },
    imageMeteo: {
        marginTop: '10px',
        width: '73px',
        height: '60px'
    },
    temperature: {
        position: 'absolute',
        top: '25px',
        left: '-35px',
        fontSize: '20px'

    },
    blanc: {
        position: 'absolute',
        right: '-70px',
        top: '-16px',
        width: '230px',
        height: '250px',
        borderRadius: '120px',
        backgroundColor: 'white'
    }
});

export default Meteo;