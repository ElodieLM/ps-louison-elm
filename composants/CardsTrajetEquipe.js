import React from 'react';
import { Text, StyleSheet, Image, View } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import CardTrajet from '../composants/CardTrajet';
import profil from '../data/profil';
import CardEquipe from '../composants/CardEquipe';

const CardTrajetEquipe = () => {
    return (
        <View style={styles.cardStyle}>
            <CardTrajet />
            <CardEquipe />
        </View>
    )
}

const styles = StyleSheet.create({
    cardStyle: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: '40px',
    }
});

export default CardTrajetEquipe;