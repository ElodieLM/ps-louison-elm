import React from 'react';
import { StyleSheet, Image, View, Text, TouchableOpacity } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import { useNavigation } from '@react-navigation/native';

const TrajetCard = ({ screenName, jour, trajet }) => {
    const navigation = useNavigation();

    return (

        <TouchableOpacity onPress={() => navigation.navigate('ModificationCarteTrajet', [jour, trajet])}>
            <View style={styles.main}>
                <View style={styles.txtMeteo}>
                    <Text style={{ fontSize: '20px', marginTop: '18px', marginBottom: '15px' }}>{jour}</Text>
                    <Text style={{ fontSize: '16px', marginBottom: '7px' }}>{trajet}</Text>
                </View>
                <View>
                    <Image source={require('../assets/more.png')} style={styles.ImageIconStyle} />
                </View>
            </View>
        </TouchableOpacity>

    )
}

export default TrajetCard;

const styles = StyleSheet.create({
    main: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 'calc( 100% - 20px)',
        height: '100px',
        marginTop: '20px',
        backgroundColor: APP_COLORS.secondaryBlue,
        marginLeft: '10px',
        marginRight: '10px',
        borderRadius: '15px',
        paddingLeft: '15px',
        paddingRight: '15px',
        overflow: 'hidden',
    },
    button: {
        backgroundColor: '#859a9b',
        borderRadius: 20,
        padding: 10,
        marginBottom: 20,
        shadowColor: '#303838',
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 10,
        shadowOpacity: 0.35,
    },
    blanc: {
        position: 'absolute',
        right: '-70px',
        top: '-16px',
        width: '230px',
        height: '250px',
        borderRadius: '120px',
        backgroundColor: 'white'
    },
    ImageIconStyle: {
        position: "absolute",
        width: "20px",
        height: "20px",
        top: "50%",
        right: "10px",
        transform: [{ translateY: '-50%' }]
    }
});