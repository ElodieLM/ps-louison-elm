import React from 'react';
import { StyleSheet, Image, View, Button, ScrollView } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';
import Bilan from '../composants/Bilan';
import Titre from '../composants/Titre';

const ListeBilans = () => {
    return (
        <View style={styles.bilans}>
            <Titre style={styles.titre} />
            <ScrollView horizontal={true}>
                <Bilan />
                <Bilan />
                <Bilan />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    bilans: {
        marginRight: '10px',
        marginLeft: '10px',
        marginTop: '50px',
        marginBottom: '50px',
    },
    titre: {
        marginBottom: '10px',
    }
});

export default ListeBilans;