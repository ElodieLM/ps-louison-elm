import React, { useState } from 'react';
import { Text, StyleSheet, Image, View, Button, Picker, CheckBox } from 'react-native';
import APP_COLORS from '../constantes/Colors';

const CarteEditionItineraire = () => {

    const [trajetSelectedValue, setTrajetSelectedValue] = useState("velotaf");
    const [typeSelectedValue, setTypeSelectedValue] = useState("java");
    const [isBoxChecked, setBoxChecked] = useState(false);

    return (
        <View style={{ marginLeft: "20px", marginRight: "20px", justifyContent: "center" }}>
            <View style={styles.cardTrajetStyle}>
                <Text style={{ marginBottom: "8px" }}>Quel était l'itinéraire ?</Text>

                <View style={styles.container}>
                    <Picker
                        selectedValue={trajetSelectedValue}
                        style={styles.inputTrajet}
                        onValueChange={(itemValue, itemIndex) => setTrajetSelectedValue(itemValue)}
                    >
                        <Picker.Item label="Vélotaf" value="velotaf" />
                        <Picker.Item label="Trajet 2" value="trajet2" />
                    </Picker>
                </View>


                <Text style={{ marginBottom: "8px" }}>Ce trajet correspond à :</Text>

                <View style={styles.container}>
                    <Picker
                        selectedValue={typeSelectedValue}
                        style={styles.inputTrajet}
                        onValueChange={(itemValue, itemIndex) => setTypeSelectedValue(itemValue)}
                    >
                        <Picker.Item label="Aller" value="aller" />
                        <Picker.Item label="Retour" value="retour" />
                        <Picker.Item label="Aller-Retour" value="allerRetour" />
                    </Picker>
                </View>

                <View style={{ flexDirection: "row" }}>
                    <CheckBox
                        value={isBoxChecked}
                        onValueChange={setBoxChecked}
                        Text="Enregistrer ce trajet pour toute la semaine"
                    />
                    <Text style={{ marginLeft: "5px", maxWidth: "250px", width: "100%" }}>Enregistrer ce trajet pour toute la semaine</Text>
                </View>
            </View>

        </View >



    )

}

const styles = StyleSheet.create({
    cardTrajetStyle: {
        position: 'relative',
        backgroundColor: APP_COLORS.secondaryBlue,
        width: '100%',
        borderRadius: '15px',
        paddingTop: "30px",
        paddingBottom: "30px",
        paddingRight: "20px",
        paddingLeft: "20px",
        marginBottom: "20px",
        marginRight: "auto",
        marginLeft: "auto"
    },
    inputTrajet: {
        width: "250px",
        padding: "7px",
        borderRadius: "15px",
        marginBottom: "20px",
    },
    imageTrajet: {
        height: '70px',
        width: '70px',
        margin: '5px'
    },
    resultat: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        left: '55px',
        top: '-10px'
    },
    points: {
        fontSize: '25px',
    },
    unite: {
        fontSize: '16px',
        marginLeft: '4px'
    },
    bouton: {
        position: 'absolute',
        bottom: '25px',
        left: '50%',
        transform: [{ translate: '-50%' }]
    }
});




export default CarteEditionItineraire;