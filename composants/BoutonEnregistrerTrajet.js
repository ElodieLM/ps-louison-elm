import React from 'react';
import { StyleSheet, Image, View, Button, Text, TouchableOpacity } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';

const BoutonEnregistrerTrajet = () => {
    return (
        <Button
            style={styles.bouton}
            title="lalala"
            onPress={() => navigation.navigate('Accueil')}
        />

        // <TouchableOpacity onPress={() => navigation.navigate('Accueil')} style={styles.bouton}>
        //     <Text></Text>
        // </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    bouton: {
        backgroundColor: APP_COLORS.primaryYellow,
        borderRadius: '30px',
        width: '50px',
        height: '50px'
    },
});

export default BoutonEnregistrerTrajet;