import React from 'react';
import { Text, StyleSheet, Image, View } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';

const CardData = (params) => {
    return (
        <View style={styles.cardStyle}>
            <Image style={styles.cardIcon} source={require('../assets/images/icons/tree.png')} />
            <Text style={{ color: APP_COLORS.white, fontSize: 24 }}>{params.value}</Text >
            <Text style={{ color: APP_COLORS.white, fontSize: 16, marginTop: 7 }}>{params.unite}</Text >
        </View>
    )
}

const styles = StyleSheet.create({
    cardStyle: {
        alignItems: "center",
        backgroundColor: APP_COLORS.primaryDarkBlue,
        width: '110px',
        borderRadius: '15px',
        padding: '10px',
        paddingBottom: '20px'

    },
    cardIcon: {
        width: '25px',
        height: '25px',
        margin: '10px'
    }
});

export default CardData;