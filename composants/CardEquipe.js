import React from 'react';
import { Text, StyleSheet, Image, View, Button } from 'react-native';
import APP_COLORS from '../constantes/Colors';
import profil from '../data/profil';

const CardEquipe = () => {
    return (
        <View style={styles.cardEquipeStyle}>
            <Image style={styles.imageEquipe} source={require('../assets/images/illu-equipe.png')} />
            <View style={styles.resultat}>
                <Text style={styles.points}>405</Text>
                <Text style={styles.unite}>pts</Text>
            </View>
            <View style={styles.bouton}>
                <Button
                    title="Equipe"
                    onPress={() => navigation.navigate('Equipe')}
                />
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    cardEquipeStyle: {
        backgroundColor: APP_COLORS.primaryBlue,
        width: '45%',
        height: '200px',
        borderRadius: '15px',
    },
    imageEquipe: {
        position: 'absolute',
        margin: '5px',
        height: '110px',
        width: '90px',
        right: '0',
        top: '10px',
    },
    resultat: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        left: '20px',
        top: '42px'
    },
    points: {
        fontSize: '25px',
    },
    unite: {
        fontSize: '16px',
        marginLeft: '4px'
    },
    bouton: {
        position: 'absolute',
        bottom: '25px',
        left: '50%',
        transform: [{ translate: '-50%' }]
    }


});

export default CardEquipe;
