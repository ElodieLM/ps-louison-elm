import { createStore, combineReducers } from 'redux';
import { countReducer } from "./countReducer";

export default createStore(
  combineReducers({
    count: countReducer, 
    test: (state = {titre:"titre1"}, action) => state
  })
);