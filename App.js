import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-ionicons';
import Ionicons from 'react-native-ionicons';

import StackAccueil from './ecrans/Accueil/StackAccueil';
import StackTrajets from './ecrans/Trajets/StackTrajets';
import StackEquipe from './ecrans/Equipe/StackEquipe';
import StackProfil from './ecrans/Profil/StackProfil';
import APP_COLORS from './constantes/Colors';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator screenOptions={{ headerShown: false }}>
        <Tab.Screen name="Accueil" component={StackAccueil} />
        <Tab.Screen name="Trajets" component={StackTrajets} />
        <Tab.Screen name="Equipe" component={StackEquipe} />
        <Tab.Screen name="Profil" component={StackProfil} />
      </Tab.Navigator>
    </NavigationContainer>
    // <NavigationContainer>
    //   <Tab.Navigator
    //     screenOptions={({ route }) => ({
    //       tabBarIcon: ({ focused, color, size }) => {
    //         let iconName;

    //         if (route.name === 'Accueil') {
    //           iconName = focused
    //             ? 'ios-information-circle'
    //             : 'ios-information-circle-outline';
    //         } else if (route.name === 'Accueil') {
    //           iconName = focused ? 'ios-list-box' : 'ios-list';
    //         }

    //         // You can return any component that you like here!
    //         return <Ionicons name={iconName} size={size} color={color} />;
    //       },
    //     })}
    //     tabBarOptions={{
    //       activeTintColor: APP_COLORS.primaryYellow,
    //       inactiveTintColor: 'gray',
    //     }}
    //   >
    //     <Tab.Screen name="Accueil" component={StackAccueil} />
    //     <Tab.Screen name="Trajets" component={StackTrajets} />
    //   </Tab.Navigator>
    // </NavigationContainer>
  )
}